# https://blog.programster.org/qemu-img-cheatsheet

# Internal Snapshots

create(){
qemu-img snapshot -c $SNAPSHOT_NAME $DISK_IMAGE
}

list(){
qemu-img snapshot -l $DISK_IMAGE.qcow2
}

restore(){
qemu-img snapshot -a $SNAPSHOT_NAME $DISK_IMAGE
}